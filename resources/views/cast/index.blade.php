@extends('layouts.master')
@section('content') 
<div class="ml-3 mr-3">
    <div class="card">
        <div class="card-header">
          <h3 class="card-title">Post Table</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            @if(session('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>
            @endif
            <a class="btn btn-primary mb-2" href="/cast/create">Create New Post</a>
          <table class="table table-bordered">
            <thead>
              <tr>
                <th style="width: 10px">No</th>
                <th>Nama</th>
                <th>Umur</th>
                <th>Biodata</th>
                <th style="width: 40px">Action</th>
              </tr>
            </thead>
            <tbody>
              {{-- @foreach ($cast as $key => $cast) --}}
              @forelse($cast as $key => $cast)
              <tr>
                  <td>{{$key + 1}}</td>
                  <td>{{$cast->name}}</td>
                  <td>{{$cast->umur}}</td>
                  <td>{{$cast->bio}}</td>
                  <td style="display: flex;">
                    <a class="btn btn-info btn-sm mr-1" href="/cast/{{$cast->id}}">Show</a>
                    <a class="btn btn-primary btn-sm mr-1" href="/cast/{{$cast->id}}/edit">Edit</a>
                    <form action="/cast/{{$cast->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                    </form>
                </td>
              </tr>
              @empty
                  <tr>
                      <td colspan="5" align="center">No Post</td>
                  </tr>
              @endforelse
            </tbody>
          </table>
        </div>
      </div>
</div>

@endsection