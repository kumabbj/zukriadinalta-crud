@extends('layouts.master')
@section('content') 
    <div class="card card-info">
        <div class="card-header">
          <h3 class="card-title">Edit Data Cast Id {{$cast->id}}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form  role="form" action="/cast/{{$cast->id}}" method="post">
            @csrf
            @method('PUT')
          <div class="card-body">
            <div class="form-group">
              <label for="name">Name</label>
              <input type="text" class="form-control" id="name" name="name" value="{{old('name', $cast->name)}}"  placeholder="Masukan Name">
              @error('name')
              <div class="alert alert-danger">{{ $message }}</div>
              @enderror
            </div>
            <div class="form-group">
              <label for="umur">Umur</label>
              <input type="text" class="form-control" id="umur" name="umur" value="{{old('umur', $cast->umur)}}" placeholder="Masukan Umur">
              @error('umur')
              <div class="alert alert-danger">{{ $message }}</div>
              @enderror
            </div>
            <div class="form-group">
                <label for="bio">Biodata</label>
                <input type="text" class="form-control" id="bio" name="bio" value="{{old('bio', $cast->bio)}}" placeholder="Masukan Bio">
                @error('bio')
              <div class="alert alert-danger">{{ $message }}</div>
              @enderror
              </div>
          </div>
          <!-- /.card-body -->
    
          <div class="card-footer">
            <button type="submit" class="btn btn-info">Update Data</button>
          </div>
        </form>
      </div>
</div>

@endsection