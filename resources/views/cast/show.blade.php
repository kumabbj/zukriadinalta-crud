@extends('layouts.master')
@section('content') 
<div class="card card-primary card-outline mt-3 ml-5 mr-5">
    <div class="card-body box-profile">
      <div class="text-center">
        <img class="profile-user-img img-fluid img-circle" src="{{asset('/adminlte/dist/img/user4-128x128.jpg')}}" alt="User profile picture">
      </div>

      <h3 class="profile-username text-center">Nama Saya {{$cast->name}}</h3>
      <h4 class="text text-center">Umur Saya {{$cast->umur}}</h4>
      <h4 class="text text-center">Biodata: <br></h4>
      <p class="text-muted text-center">{{$cast->bio}}</p>
    </div>
    <!-- /.card-body -->
  </div>
@endsection