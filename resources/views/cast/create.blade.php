@extends('layouts.master')
@section('content') 
<div class="ml-3 mr-3">
    <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Create New Cast</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form  role="form" action="/cast" method="post">
            @csrf
          <div class="card-body">
            <div class="form-group">
              <label for="name">Name</label>
              <input type="text" class="form-control" id="name" name="name" value="{{old('name', '')}}"  placeholder="Masukan Name">
              @error('name')
              <div class="alert alert-danger">{{ $message }}</div>
              @enderror
            </div>
            <div class="form-group">
              <label for="umur">Umur</label>
              <input type="text" class="form-control" id="umur" name="umur" value="{{old('umur', '')}}" placeholder="Masukan Umur">
              @error('umur')
              <div class="alert alert-danger">{{ $message }}</div>
              @enderror
            </div>
            <div class="form-group">
                <label for="bio">Biodata</label>
                <input type="text" class="form-control" id="bio" name="bio" value="{{old('bio', '')}}" placeholder="Masukan Bio">
                @error('bio')
              <div class="alert alert-danger">{{ $message }}</div>
              @enderror
              </div>
          </div>
          <!-- /.card-body -->
    
          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Create New</button>
          </div>
        </form>
      </div>
</div>

@endsection