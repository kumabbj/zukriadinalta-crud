<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/cast/create', 'PostController@create');
Route::post('/cast', 'PostController@store');
Route::get('/cast', 'PostController@index');
Route::get('/cast/{cast_id}', 'PostController@show');
Route::get('/cast/{cast_id}/edit', 'PostController@edit');
Route::put('/cast/{cast_id}',  'PostController@update');
Route::delete('/cast/{cast_id}',  'PostController@destory');
