<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class PostController extends Controller
{
    public function create(){
        return view('cast.create');
    }

    public function store(Request $request){
        $request->validate([ 
            'name' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        $query = DB::table('cast')->insert([
        'name'=> $request['name'],
        'umur' => $request['umur'],
        'bio' => $request['bio']
        ]);
        return redirect('/cast')->with('success', 'Data Berhasil Disimpan');
    }

    public function index(){
        $cast = DB::table('cast')->get(); // sama dengan => select * form cast;
        // dd($cast);
        return view('cast.index', compact('cast'));
    }

    public function show($id){
        $cast = DB::table('cast')->where('id', $id)->first();
        // dd($cast);
        return view('cast.show', compact('cast'));
    }

    public function edit($id){
        $cast = DB::table('cast')->where('id', $id)->first();

        return view('cast.edit', compact('cast'));

    }

    public function update($id, Request $request){
        $request->validate([ 
            'name' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        $query = DB::table('cast')
                ->where('id', $id)
                ->update([
                    'name'=> $request['name'],
                    'umur' => $request['umur'],
                    'bio' => $request['bio']
                ]);
        return redirect('/cast')->with('success', 'Data Berhasil Update');
    }

    public function destory($id){
        $query = DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast')->with('success', 'Data Berhasil Di Hapus');
    }
}
